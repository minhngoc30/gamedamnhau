using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossZombie : MonoBehaviour
{
    public int Health;
    private int currenHealth;
    public heathBar hearthbar;
    public GameObject hook;
    // Start is called before the first frame update
    void Start()
    {
        currenHealth = Health;
        hearthbar.SetMaxHealth(Health);
    }

    // Update is called once per frame
    void Update()
    {
        hearthbar.SetHealth(currenHealth);
        {
            if (currenHealth <= 0)
            {
                Destroy(gameObject);
                Instantiate(hook, hook.transform.position, Quaternion.identity);
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag=="bullet" || collision.gameObject.tag=="bullet2")
        {

            currenHealth -= 1;
        }
    }
}
