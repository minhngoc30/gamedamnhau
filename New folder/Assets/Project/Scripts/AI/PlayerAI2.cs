using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerAI2 : MonoBehaviour
{
    [Header("Point Random")]
    public GameObject[] waypoint;
    private int waypoinindex;
    private float move, time,time2=-1;
    private bool faceright = true;
    private Animator anim;

    [Header("GunBullet")]
    Vector2 Direction;
    public GameObject Gun, tar;
    public GameObject Bullet, efectbullet, item;
    public float FireRate, Foces;
    float nextTimeToFire;
    public Transform pointt;
    private Vector3 targets;

    [Header("Health")]
    public SpriteRenderer sprite;
    private Transform returnposition;
    public int Health;
    private int currenHealth;
    public heathBar hearthbar;
    private Sound sound;
    public GameObject  im;
    private float timeefectdie = -1;
    public float numberpldie;
    [SerializeField] Text Textnumberdie;

    void Start()
    {
        transform.position = waypoint[waypoinindex].transform.position;
        anim = GetComponent<Animator>();
        currenHealth = Health;
        hearthbar.SetMaxHealth(Health);
        returnposition = GameObject.FindGameObjectWithTag("pointRestar2").transform;
        sound = GameObject.FindGameObjectWithTag("sound").GetComponent<Sound>();

    }

    void Update()
    {
        Textnumberdie.text = numberpldie.ToString("0");

        if (waypoinindex == waypoint.Length) waypoinindex = 0;
        point();

        if (transform.position.x > waypoint[waypoinindex].transform.position.x && faceright)
        {
            Flip();
        }
        if (transform.position.x < waypoint[waypoinindex].transform.position.x && !faceright)
        {
            Flip();
        }
        //heath
        hearthbar.SetHealth(currenHealth);
        if (currenHealth <= 0)
        {
            sound.Playsound("pldie");
            GameObject newArrow = Instantiate(item, transform.position, transform.rotation);
            transform.position = returnposition.position;
            currenHealth = Health;
            waypoinindex = 0;
            numberpldie += 1;
            im.SetActive(true);
            timeefectdie = 1;
        }
        if (timeefectdie > 0) timeefectdie -= Time.deltaTime;
        if (timeefectdie < 0) { im.SetActive(false); timeefectdie = -1; }
        if (currenHealth > 20) currenHealth = 20;
    }
    void point()
    {
        if (time2 < 0)
        {
            if (time == 5)
            {
                transform.position = Vector2.MoveTowards(transform.position, transform.position, move * Time.deltaTime);
                anim.SetBool("run", false);

            }
            if (time >= 0)
            {
                FindClosestenemy();
            }
            if (move == 0) time -= Time.deltaTime;
            if (time < 0)
            {
                efectbullet.SetActive(false);
                anim.SetBool("run", true);
                move = 5;
                transform.position = Vector2.MoveTowards(transform.position, waypoint[waypoinindex].transform.position, move * Time.deltaTime);
            }
        }
        else if (time2 > 0)
        {

            transform.position = Vector2.MoveTowards(transform.position, transform.position, move * Time.deltaTime);
            anim.SetBool("run", false);
            FindPlane();
        }
        if (waypoinindex == 0 && transform.position == waypoint[waypoinindex].transform.position)
            waypoinindex = Random.Range(0, 4);
        if (waypoinindex == 1 && transform.position == waypoint[waypoinindex].transform.position)
            waypoinindex = Random.Range(6, 5);
        if (waypoinindex == 2 && transform.position == waypoint[waypoinindex].transform.position)
            waypoinindex = Random.Range(14, 19);
        if (waypoinindex == 3 && transform.position == waypoint[waypoinindex].transform.position)
            waypoinindex = 26;
        if (waypoinindex == 4 && transform.position == waypoint[waypoinindex].transform.position)
            waypoinindex = 5;
        if (waypoinindex == 5 && transform.position == waypoint[waypoinindex].transform.position)
            waypoinindex = Random.Range(5, 7);
        if (waypoinindex == 6 && transform.position == waypoint[waypoinindex].transform.position)
            waypoinindex = Random.Range(8, 15);
        if (waypoinindex == 7 && transform.position == waypoint[waypoinindex].transform.position)
            waypoinindex = 26;
        if (waypoinindex == 8 && transform.position == waypoint[waypoinindex].transform.position)
            waypoinindex = Random.Range(20, 25);
        if (waypoinindex == 9 && transform.position == waypoint[waypoinindex].transform.position)
            waypoinindex = 2;
        if (waypoinindex == 10 && transform.position == waypoint[waypoinindex].transform.position)
            waypoinindex = 6;
        if (waypoinindex == 11 && transform.position == waypoint[waypoinindex].transform.position)
            waypoinindex = Random.Range(20, 25);
        if (waypoinindex == 12 && transform.position == waypoint[waypoinindex].transform.position)
            waypoinindex = Random.Range(12, 19);
        if (waypoinindex == 13 && transform.position == waypoint[waypoinindex].transform.position)
            waypoinindex = Random.Range(12, 19);
        if (waypoinindex == 14 && transform.position == waypoint[waypoinindex].transform.position)
            waypoinindex = 26;
        if (waypoinindex == 15 && transform.position == waypoint[waypoinindex].transform.position)
            waypoinindex = Random.Range(12, 19);
        if (waypoinindex == 16 && transform.position == waypoint[waypoinindex].transform.position)
            waypoinindex = 2;
        if (waypoinindex == 17 && transform.position == waypoint[waypoinindex].transform.position)
            waypoinindex = 6;
        if (waypoinindex == 18 && transform.position == waypoint[waypoinindex].transform.position)
            waypoinindex = 6;
        if (waypoinindex == 19 && transform.position == waypoint[waypoinindex].transform.position)
            waypoinindex = Random.Range(12, 19);
        if (waypoinindex == 20 && transform.position == waypoint[waypoinindex].transform.position)
            waypoinindex = Random.Range(8, 11);
        if (waypoinindex == 21 && transform.position == waypoint[waypoinindex].transform.position)
            waypoinindex = 26;
        if (waypoinindex == 22 && transform.position == waypoint[waypoinindex].transform.position)
            waypoinindex = 6;
        if (waypoinindex == 23 && transform.position == waypoint[waypoinindex].transform.position)
            waypoinindex = Random.Range(8, 11);
        if (waypoinindex == 24 && transform.position == waypoint[waypoinindex].transform.position)
            waypoinindex = Random.Range(20, 23);
        if (waypoinindex == 25 && transform.position == waypoint[waypoinindex].transform.position)
            waypoinindex = Random.Range(20, 23);
        if (waypoinindex == 26 && transform.position == waypoint[waypoinindex].transform.position)
            waypoinindex = Random.Range(20, 23);

        if (transform.position != waypoint[waypoinindex].transform.position)
        {
            transform.position = Vector2.MoveTowards(transform.position, waypoint[waypoinindex].transform.position, move * Time.deltaTime);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
       
        if (collision.gameObject.tag == "enemy" || collision.gameObject.tag == "Player1"|| collision.gameObject.tag == "PlayerReal1")
        {
            transform.position = Vector2.MoveTowards(transform.position, transform.position, 0 * Time.deltaTime);
            time = 5;
            move = 0;
        }
        if (collision.gameObject.tag == "bulet" || collision.gameObject.tag == "enemyAtack")
        {
            StartCoroutine(FlashRed());
            currenHealth -= 1;
        }
       
        if (collision.gameObject.tag == "point18" &&tank.number == 4)
        {
            move=0;
            transform.position = Vector2.MoveTowards(transform.position, transform.position, 0 * Time.deltaTime);
            time2 = 20;
        }
        if (collision.gameObject.tag == "item")
        {
            currenHealth += 5;
        }
    }
    public void Flip()
    {
        faceright = !faceright;
        Vector3 Scale;
        Scale = transform.localScale;
        Scale.x *= -1;
        transform.localScale = Scale;
    }
    void shoot(Vector2 direction, float rotation)
    {
        GameObject BulletIns = Instantiate(Bullet, pointt.position, pointt.rotation);
        //     BulletIns.GetComponent<Rigidbody2D>().AddForce(Direction * Foces );
        BulletIns.GetComponent<Rigidbody2D>().velocity = direction * Foces;
    }

    void FindClosestenemy()
    {
        float distanceenemy = Mathf.Infinity;
        bossAI closestEnemy = null;
        bossAI[] allAnemy = GameObject.FindObjectsOfType<bossAI>();

        foreach (bossAI currentEnemy in allAnemy)
        {
            float distanceToEnemy = (currentEnemy.transform.position - this.transform.position).sqrMagnitude;
            if (distanceToEnemy < distanceenemy)
            {
                distanceenemy = distanceToEnemy;
                closestEnemy = currentEnemy;
                Vector2 targetPos = closestEnemy.transform.position;
                Direction = (targetPos - (Vector2)transform.position) * 2f;

            }
        }
        PlayerAI1 closestPlayer1 = null;
        PlayerAI1[] allPlayer1 = GameObject.FindObjectsOfType<PlayerAI1>();
        foreach (PlayerAI1 currentPlayer1 in allPlayer1)
        {
            float distanceToPlayer1 = (currentPlayer1.transform.position - this.transform.position).sqrMagnitude;
            if (distanceToPlayer1 < distanceenemy)
            {
                distanceenemy = distanceToPlayer1;
                closestPlayer1 = currentPlayer1;
                Vector2 targetPosPlayer = closestPlayer1.transform.position;
                Direction = (targetPosPlayer - (Vector2)transform.position) * 2f;
            }
        }
        pl closestpl = null;
        pl[] allpl = GameObject.FindObjectsOfType<pl>();
        foreach (pl currentpl in allpl)
        {
            float distanceTopl = (currentpl.transform.position - this.transform.position).sqrMagnitude;
            if (distanceTopl < distanceenemy)
            {
                distanceenemy = distanceTopl;
                closestpl = currentpl;
                Vector2 targetPosPlayer = closestpl.transform.position;
                Direction = (targetPosPlayer - (Vector2)transform.position) * 2f;
            }
        }
        Gun.transform.up = Direction;

        //bullet
        targets = tar.transform.position;
        Vector3 difference = targets - Gun.transform.position;
        float rotation = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
        float distance = difference.magnitude;
        Vector2 direction = difference / distance;
        direction.Normalize();
        if (Time.time > nextTimeToFire)
        {
            nextTimeToFire = Time.time + 1 / FireRate;
            shoot(direction, rotation);
        }
        efectbullet.SetActive(true);
    Debug.DrawLine(this.transform.position, closestEnemy.transform.position);
    }
    void FindPlane()
    {
        float distancePlane = Mathf.Infinity;
        Plane closestPlane = null;
        Plane[] allPlane = GameObject.FindObjectsOfType<Plane>();

        foreach (Plane currentPlane in allPlane)
        {
            float distanceToPlane = (currentPlane.transform.position - this.transform.position).sqrMagnitude;
            if (distanceToPlane < distancePlane)
            {
                distancePlane = distanceToPlane;
                closestPlane = currentPlane;
                Vector2 targetPos = closestPlane.transform.position;
                Direction = (targetPos - (Vector2)transform.position) * 2f;

            }
        }
        Gun.transform.up = Direction;

        //bullet
        targets = tar.transform.position;
        Vector3 difference = targets - Gun.transform.position;
        float rotation = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
        float distance = difference.magnitude;
        Vector2 direction = difference / distance;
        direction.Normalize();

        if (Time.time > nextTimeToFire)
        {
            nextTimeToFire = Time.time + 1 / FireRate;
            shoot(direction, rotation);
        }
        efectbullet.SetActive(true);
        Debug.DrawLine(this.transform.position, closestPlane.transform.position);
    }
    public IEnumerator FlashRed()
    {
        sprite.color = Color.red;
        yield return new WaitForSeconds(0.1f);
        sprite.color = Color.white;
    }
   
}
