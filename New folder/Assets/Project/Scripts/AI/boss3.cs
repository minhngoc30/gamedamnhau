using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class boss3 : MonoBehaviour
{
    public GameObject enemy ,hook;
    public int heath, curenheath;
    public heathBar heathbar;
    // Start is called before the first frame update
    void Start()
    {
        heathbar.SetMaxHealth(curenheath);
        heath = curenheath;
    }

    // Update is called once per frame
    void Update()
    {
        heathbar.SetHealth(curenheath);
        if (curenheath <= 0)
        {
            Instantiate(hook, hook.transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag=="bullet2"||collision.gameObject.tag=="bullet")
        {
            curenheath -= 1;
            Instantiate(enemy, enemy.transform.position, Quaternion.identity);
            
        }
    }
}
