using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bossAI : MonoBehaviour
{
    [Header("Player Target")]
    [SerializeField] private GameObject[] players;
    [SerializeField] private Transform playerTarget;

    [Header("Distance Target")]
    [SerializeField] private float distanceAttack;
    [SerializeField] private float distanceBarrier;
    [SerializeField] private float distanceAvoidBarrier;
    [SerializeField] private bool[] isAttackDistance;


    [Header("AI Enemies")]
    [Range(0, 6)] public float moveSpeed;
    [Range(0, 6)] public float acceleration;
    [SerializeField] private bool isFindTarget;
    [SerializeField] private bool isBreak;
    [SerializeField] private bool isAvoid;
    [SerializeField] private bool isUP;
    [SerializeField] private bool isRight = true;
    [SerializeField] private Vector3 randomTarget;

    private Animator anim;
    //heath
    public int Health;
    private int currenHealth;
    public heathBar hearthbar;
    public static float numberenemy;
    public static float numberenemy2;
    public Collider2D trigger;
    public Collider2D trigger2;
    public GameObject xu, effect;
    float nextTimeToFire;
    private void Start()
    {
        numberenemy = PlayerPrefs.GetInt("numberenemy");
        numberenemy2 = PlayerPrefs.GetInt("numberenemy2");
        anim = gameObject.GetComponentInChildren<Animator>();
        StartCoroutine(UpdateListPlayer());
        StartCoroutine(RandomPosTagert());
        StartCoroutine(RandomAcceleration());
        StartCoroutine(RandomBreak());
        currenHealth = Health;
        hearthbar.SetMaxHealth(Health);
    }
    private void Update()
    {
        //heath
        hearthbar.SetHealth(currenHealth);
        if (currenHealth <= 0)
        {
            trigger.enabled = false;
            trigger2.enabled = false;
            anim.SetBool("die", true);
            Destroy(gameObject, 0.5f);

            if (Time.time > nextTimeToFire)
            {
                nextTimeToFire = Time.time + 1;
                GameObject newEffect = Instantiate(effect, transform.position, transform.rotation);
                GameObject newArrow = Instantiate(xu, transform.position, transform.rotation);
            }
        }
     
        if (players.Length != 0)
        {
            for (int i = 0; i < players.Length; i++)
            {
                float distance = Vector2.Distance(transform.position, players[i].transform.position);

                if (distance <= distanceAttack)
                {
                    isAttackDistance[i] = true;
                }
                else
                {
                    isAttackDistance[i] = false;
                }
            }
        }
        if (isRight)
        {
            RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector3.right);
            if (hit.collider != null)
            {
                if (hit.collider.gameObject.CompareTag("barrier"))
                {
                    distanceBarrier = Vector3.Distance(transform.position, hit.transform.position);
                    if (distanceBarrier <= distanceAvoidBarrier)
                    {
                        isAvoid = true;
                    }
                    else
                    {
                        isAvoid = false;
                    }
                    Debug.Log(distanceBarrier);
                }
                else
                {
                    isAvoid = false;
                }

            }
        }
        else
        {
            RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector3.left);
            if (hit.collider != null)
            {
                if (hit.collider.gameObject.CompareTag("barrier"))
                {
                    distanceBarrier = Vector3.Distance(transform.position, hit.transform.position);
                    if (distanceBarrier <= distanceAvoidBarrier)
                    {
                        isAvoid = true;
                    }
                    else
                    {
                        isAvoid = false;
                    }
                   // Debug.Log(distanceBarrier);
                }
                else
                {
                    isAvoid = false;
                }
            }
        }


        MoveToPTarget();
        Flip();
    }
    private void MoveToPTarget()
    {
        if (playerTarget != null)
        {
            if (Vector2.Distance(playerTarget.position, transform.position) > distanceAttack * 1.3f)
            {
                playerTarget = null;
                isFindTarget = false;
                return;
            }
            else
            {
                isFindTarget = true;
            }
            if (isFindTarget)
            {
                Vector3 myPos = transform.position;

                if (isAvoid)
                {
                    myPos.y += moveSpeed * acceleration * Time.deltaTime;
                    myPos.z = 1f;
                    transform.position = myPos;
                    Debug.Log("Avoid Up");
                }
                else
                {
                    myPos = Vector3.MoveTowards(transform.position, playerTarget.position, moveSpeed * Time.deltaTime);
                    myPos.z = 1f;

                    transform.position = myPos;
                    anim.SetBool("run", true);
                }
            }

        }
        else
        {
            for (int i = 0; i < players.Length; i++)
            {
                if (isAttackDistance[i])
                {
                    playerTarget = players[i].transform;
                    return;
                }
            }
            if (isFindTarget == false)
            {
                if (isBreak)
                {
                    anim.SetBool("run", false);
                    return;
                }
                Vector3 myPos = transform.position;
                myPos = Vector3.MoveTowards(transform.position, randomTarget, moveSpeed * Time.deltaTime);
                myPos.z = 1f;
                transform.position = myPos;
                float dis = Vector3.Distance(transform.position, randomTarget);
                anim.SetBool("run", true);
            }
        }
    }
    private void Flip()
    {
        if (isFindTarget)
        {
            if (playerTarget.position.x - transform.position.x > 0)
            {
                transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
                isRight = true;
            }
            else if (playerTarget.position.x - transform.position.x < 0)
            {
                transform.localScale = new Vector3(-0.2f, 0.2f, 0.2f);
                isRight = false;
            }
        }
        else
        {
            if (randomTarget.x - transform.position.x > 0)
            {
                transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
                isRight = true;
            }
            else if (randomTarget.x - transform.position.x < 0)
            {
                transform.localScale = new Vector3(-0.2f, 0.2f, 0.2f);
                isRight = false;
            }
        }
    }
    private IEnumerator UpdateListPlayer()
    {
       
            players = GameObject.FindGameObjectsWithTag("Player");
            yield return new WaitForSeconds(2f);
            StartCoroutine(UpdateListPlayer());
          
        
    }
    private IEnumerator RandomPosTagert()
    {
        Vector2 pos = new Vector2(Random.Range(-100, 100), Random.Range(-21, 58));
        randomTarget = pos;
        yield return new WaitForSeconds(6f);
        StartCoroutine(RandomPosTagert());
    }
    private IEnumerator RandomAcceleration()
    {
        acceleration = Random.Range(0.7f, 1.7f);
        yield return new WaitForSeconds(4f);
        StartCoroutine(RandomAcceleration());
    }
    private IEnumerator RandomBreak()
    {
        isBreak = true;
        yield return new WaitForSeconds(Random.Range(3f, 5f));
        isBreak = false;
        yield return new WaitForSeconds(Random.Range(6f, 24f));
        StartCoroutine(RandomBreak());
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "bulet" || collision.gameObject.tag=="bulet2")
        {
            if(MenuManage.gunindex1==1||MenuManage.gunindex3==1||MenuManage.gunindex0==1)
            currenHealth -= 1;
            if(MenuManage.gunindex2==1|| MenuManage.gunindex4 == 1)
                currenHealth -= 3;
        }
        if (collision.gameObject.tag == "bulet" && currenHealth < 0) { enemyRandom.numberenemy += 1; }
        if (collision.gameObject.tag == "bulet2" && currenHealth < 0) enemyRandom.numberenemy2 += 1;

    }
}
