using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class enemyRandom : MonoBehaviour
{
    public float x, x1, y, y1;
    public GameObject stars;
    float nextTimeToFire;
    public float FireRate ;
    private int thingyCount;
    public static int numberenemy;
    public static int numberenemy2;
    public Text moneyText;
    public Text moneyText2;
    public float limitEnemy;

    void Start()
    {
        numberenemy = PlayerPrefs.GetInt("numberenemy");
        numberenemy2 = PlayerPrefs.GetInt("numberenemy2");

    }
    void Update()
    {
        moneyText.text = ":" +numberenemy.ToString() + "";
        moneyText2.text = ""+numberenemy2.ToString() + ":";

        Vector3 random = new Vector3(Random.Range(x, x1), Random.Range(y, y1), 0f);
        if (Time.time > nextTimeToFire&&thingyCount< limitEnemy)
        {
            nextTimeToFire = Time.time + 1 / FireRate;
            Instantiate(stars, random, Quaternion.identity);
        }
        GameObject[] thingyToFind = GameObject.FindGameObjectsWithTag("enemy");

         thingyCount = thingyToFind.Length;
    }
}
