﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class pl : MonoBehaviour
{
    [Header("Player")]
    private Controller contr;
    public float speed;
    private Animator anim;
    private bool run;
    Vector2 move;

    [Header("Health")]
    public SpriteRenderer sprite;
    private Transform target;
    //heath
    public int Health;
    private int currenHealth;
    public heathBar hearthbar;
    public GameObject item;
    private Sound sound;
    AudioSource audioSrc;
    public Text text;
    public float numberpldie;
    public GameObject im;
    private float timeefectdie = -1;
    private void Start()
    {
        if (MenuManage.player1 == 1) { text = GameObject.Find("textpldie1").GetComponent<Text>(); im = GameObject.Find("im1"); }
        if (MenuManage.player2 == 1) { text = GameObject.Find("textpldie2").GetComponent<Text>(); im = GameObject.Find("im2"); }
        if (MenuManage.player3 == 1) { text = GameObject.Find("textpldie3").GetComponent<Text>(); im = GameObject.Find("im3"); }
        if (MenuManage.player8 == 1) { text = GameObject.Find("textpldie8").GetComponent<Text>(); im = GameObject.Find("im8"); }

        audioSrc = GetComponent<AudioSource>();
        sound = GameObject.FindGameObjectWithTag("sound").GetComponent<Sound>();
        target = GameObject.FindGameObjectWithTag("pointRestar").transform;
        contr = GameObject.FindObjectOfType<Controller>();
        anim = GetComponent<Animator>();
        currenHealth = Health;
        hearthbar.SetMaxHealth(Health);
    }
    private void Update()
    {
        text.text = numberpldie.ToString("0");

        anim.SetBool("run", run);
        if (contr.vector.x != 0)
        {
            run = true; 
            if (!audioSrc.isPlaying)
                audioSrc.Play();
        }
        else run = false;
       
        if (contr.vector.x > 0)
            transform.rotation = Quaternion.Euler(0, 0, 0);
        else if (contr.vector.x < 0)
            transform.rotation = Quaternion.Euler(0, 180, 0);
        move.x = contr.vector.x;
        move.y = contr.vector.y;
        transform.position += contr.vector * (speed * Time.deltaTime);
        //heath
        hearthbar.SetHealth(currenHealth);
        if (currenHealth <= 0)
        {
            GameObject newArrow = Instantiate(item, transform.position, transform.rotation);
            transform.position = target.position;
            currenHealth = Health;
            sound.Playsound("pldie");
            numberpldie += 1;
            im.SetActive(true);
            timeefectdie = 1;
        }
        if (currenHealth > 20) currenHealth = 20;
        if (timeefectdie > 0) timeefectdie -= Time.deltaTime;
        if (timeefectdie < 0) { im.SetActive(false); timeefectdie = -1; }
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "bulet2" ||collision.gameObject.tag== "enemyAtack")
        {
            StartCoroutine(FlashRed());
            currenHealth -= 1;
        }
        if (collision.gameObject.tag == "item")
        {
            currenHealth += 5;
            sound.Playsound("item");
        }
        if(collision.gameObject.tag=="coin")
        {
            coin.moneyAmount += 1;
        }    
    }
    public IEnumerator FlashRed()
    {
        sprite.color = Color.red;
        yield return new WaitForSeconds(0.1f);
        sprite.color = Color.white;
    }

}
