﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class pl2 : MonoBehaviour
{
    [Header("Player")]
    private Controller contr;
    public float speed;
    private Animator anim;
    private bool run;
    Vector2 move;

    [Header("Health")]
    public SpriteRenderer sprite;
    private Transform target;
    //heath
    public int Health;
    private int currenHealth;
    public heathBar hearthbar;
    public GameObject item;
    private Sound sound;
    AudioSource audioSrc;
    public Text text;
    public float numberpldie;
    public GameObject im;
    private float timeefectdie = -1;
    private void Start()
    {
        if (MenuManage.player4 == 1) { text = GameObject.Find("textpldie4").GetComponent<Text>(); im = GameObject.Find("im4"); }
        if (MenuManage.player5 == 1) { text = GameObject.Find("textpldie5").GetComponent<Text>(); im = GameObject.Find("im5"); }
        if (MenuManage.player6 == 1) { text = GameObject.Find("textpldie6").GetComponent<Text>(); im = GameObject.Find("im6"); }
        if (MenuManage.player7 == 1) { text = GameObject.Find("textpldie7").GetComponent<Text>(); im = GameObject.Find("im7"); }
        audioSrc = GetComponent<AudioSource>();
        sound = GameObject.FindGameObjectWithTag("sound").GetComponent<Sound>();
        target = GameObject.FindGameObjectWithTag("pointRestar2").transform;
        contr = GameObject.FindObjectOfType<Controller>();
        anim = GetComponent<Animator>();
        currenHealth = Health;
        hearthbar.SetMaxHealth(Health);
    }
    private void Update()
    {


        anim.SetBool("run", run);
        if (contr.vector.x != 0)
        {
            run = true;
            if (!audioSrc.isPlaying)
                audioSrc.Play();
        }
        else run = false;
        
        if (contr.vector.x > 0)
            transform.rotation = Quaternion.Euler(0, 0, 0);
        else if (contr.vector.x < 0)
            transform.rotation = Quaternion.Euler(0, 180, 0);
       // transform.Translate(contr.vector * speed * Time.deltaTime);
        move.x = contr.vector.x;
        move.y = contr.vector.y;
        //  rb.MovePosition(rb.position + move * speed * Time.deltaTime);
        transform.position += contr.vector * (speed * Time.deltaTime);
        //heath
        hearthbar.SetHealth(currenHealth);
        if (currenHealth <= 0)
        {
            GameObject newArrow = Instantiate(item, transform.position, transform.rotation);
            transform.position = target.position;
            currenHealth = Health;
            sound.Playsound("pldie");
            numberpldie += 1;
            im.SetActive(true);
            timeefectdie = 1;
        }
        if (currenHealth > 20) currenHealth = 20;
        if (timeefectdie > 0) timeefectdie -= Time.deltaTime;
        if (timeefectdie < 0) { im.SetActive(false); timeefectdie = -1; }
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "bulet" ||collision.gameObject.tag== "enemyAtack")
        {
            StartCoroutine(FlashRed());
            currenHealth -= 1;
        }
        if (collision.gameObject.tag == "item")
        {
            currenHealth += 5;
            sound.Playsound("item");
        }
        if (collision.gameObject.tag == "coin")
        {
            coin.moneyAmount += 1;
        }
    }
    public IEnumerator FlashRed()
    {
        sprite.color = Color.red;
        yield return new WaitForSeconds(0.1f);
        sprite.color = Color.white;
    }

}
