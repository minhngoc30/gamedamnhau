using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class Controller : MonoBehaviour , IPointerUpHandler,IPointerDownHandler,IDragHandler
{
    public Vector3 vector;
    public RectTransform backGroundControler, handControler;
    public float handleRange;

    public void OnPointerUp(PointerEventData eventData)
    {
        vector = Vector3.zero;
        handControler.anchoredPosition = Vector2.zero;
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        OnDrag(eventData);
    }
    public void OnDrag(PointerEventData eventData)
    {
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(backGroundControler, eventData.position, eventData.pressEventCamera, out Vector2 localPoint))
        {
            var sizeBackGround = backGroundControler.sizeDelta;
            localPoint /= sizeBackGround;

            vector = new Vector3(localPoint.x * 6f - 1, localPoint.y * 6f - 1, 0);
            vector = vector.magnitude > 1f ? vector.normalized : vector;

            float handlePos = sizeBackGround.x / 2 * handleRange;
            handControler.anchoredPosition = new Vector2(vector.x * handlePos, vector.y * handlePos);
        }
    }



    //public Transform player;
    //public float speed;

    //public Transform circle;
    //public Transform outerCircle;

    //private Vector2 startingPoint;
    //private int leftTouch = 99;

    //private void Update()
    //{
    //    int i = 0;
    //    while (i < Input.touchCount)
    //    {
    //        Touch t = Input.GetTouch(i);
    //        Vector2 touchPos = getTouchPosition(t.position) * -1;
    //        if (t.phase == TouchPhase.Began)
    //        {
    //            if (t.position.x > Screen.width / 2)
    //            {
    //               // shootBullet();
    //            }
    //            else
    //            {
    //                leftTouch = t.fingerId;
    //                startingPoint = touchPos;
    //            }
    //        }
    //        else if (t.phase == TouchPhase.Moved && leftTouch == t.fingerId)
    //        {
    //            Vector2 offset = touchPos - startingPoint;
    //            Vector2 direction = Vector2.ClampMagnitude(offset, 1.0f);

    //            moveCharacter(direction);
    //            circle.transform.position = new Vector2(outerCircle.transform.position.x + direction.x, outerCircle.transform.position.y + direction.y);
    //        } else if (t.phase == TouchPhase.Ended && leftTouch == t.fingerId) { leftTouch = 99; }
    //            ++i;

    //        }
    //    }
    //Vector2 getTouchPosition(Vector2 touchPosition)
    //{
    //    return GetComponent<Camera>().ScreenToWorldPoint(new Vector3(touchPosition.x, touchPosition.y, transform.position.z));
    //}
    //void moveCharacter(Vector2 direction)
    //{
    //    player.Translate(direction * speed * Time.deltaTime);
    //}
    //void shootBullet()
    //{
    //    GameObject b=Instantiate(bull)
    //}

}
