using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Gun : MonoBehaviour
{
    private Controller1 contr;
    private float angle;

    public GameObject pun, tar ;
    float nextTimeToFire;
    public float FireRate;

    public GameObject bulletpretabEffect;
    public float speed = 60f, time = 5;
    private Vector3 target;
    public Transform vitri;
    private Transform pl;
    private Sound sound;

    void Start()
    {
        sound = GameObject.FindGameObjectWithTag("sound").GetComponent<Sound>();

        contr = GameObject.FindObjectOfType<Controller1>();
        if (MenuManage.player1 == 1 || MenuManage.player2 == 1 || MenuManage.player3 == 1 || MenuManage.player8 == 1)
        {
            pl = GameObject.FindGameObjectWithTag("PlayerReal1").GetComponent<Transform>();
        }
        if (MenuManage.player4 == 1 || MenuManage.player5 == 1 || MenuManage.player6 == 1 || MenuManage.player7 == 1)
        {
            pl = GameObject.FindGameObjectWithTag("PlayerReal2").GetComponent<Transform>();
        }
    }

    void Update()
    {
        transform.position = pl.position;

        RotateWeapon();

        if (time < 0)
        {
            time -= Time.deltaTime;
            if (time <= -3) time = 5;
        }
        if (contr.vector.x != 0)
            bulletpretabEffect.gameObject.SetActive(true);
        else bulletpretabEffect.gameObject.SetActive(false);
    }
    void RotateWeapon()
    {
        if (contr.vector == Vector3.zero)  return; 
        angle = Mathf.Atan2(contr.vector.y, contr.vector.x) * Mathf.Rad2Deg + 90;
        var lookRotation = Quaternion.Euler(angle * Vector3.forward);
        transform.rotation = lookRotation;
        time -= Time.deltaTime;
        //bullet
        target = tar.transform.position;
        Vector3 difference = target - pun.transform.position;
        float rotation = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
        pun.transform.rotation = Quaternion.Euler(angle * Vector3.forward);
        float distance = difference.magnitude;
        Vector2 direction = difference / distance;
        direction.Normalize();
      
        if (Time.time > nextTimeToFire && time >= 0)
        {
            nextTimeToFire = Time.time + 1 / FireRate;
            firBullet(direction, rotation);
        }

    }

    void firBullet(Vector2 direction, float rotation)
    {
        if (MenuManage.player1 == 1 || MenuManage.player2 == 1 || MenuManage.player3 == 1 || MenuManage.player8 == 1)
        {
            GameObject bulletpretab = Instantiate(Resources.Load("bullet1", typeof(GameObject))) as GameObject;
            GameObject newArrow = Instantiate(bulletpretab, vitri.position, vitri.rotation);
            newArrow.GetComponent<Rigidbody2D>().velocity = direction * speed;
            cameraShake.instance.startShake(2f, .1f);
            if (MenuManage.gunindex1 == 1) 
                sound.Playsound("gunlong");
            if (MenuManage.gunindex2 == 1)
                sound.Playsound("gunshort");
            if (MenuManage.gunindex4 == 1)
                sound.Playsound("ak");
        }
        if (MenuManage.player4 == 1 || MenuManage.player5 == 1 || MenuManage.player6 == 1 || MenuManage.player7 == 1)
        {
            GameObject bulletpretab = Instantiate(Resources.Load("bullet2", typeof(GameObject))) as GameObject;
            GameObject newArrow = Instantiate(bulletpretab, vitri.position, vitri.rotation);
            newArrow.GetComponent<Rigidbody2D>().velocity = direction * speed;
            cameraShake.instance.startShake(0.2f, .1f);
            if (MenuManage.gunindex1 == 1)
                sound.Playsound("gunlong");
            if (MenuManage.gunindex2 == 1)
                sound.Playsound("gunshort");
            if (MenuManage.gunindex4 == 1)
                sound.Playsound("ak");
        }


    }
}
