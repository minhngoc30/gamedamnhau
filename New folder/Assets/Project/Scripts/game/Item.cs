using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public GameObject itemPluss;
 
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag=="Player1"||collision.gameObject.tag=="Player2")
        {
            Destroy(gameObject);
        }
        if(collision.gameObject.tag=="PlayerReal1"||collision.gameObject.tag=="PlayerReal2")
        {
            itemPluss.SetActive(true);
            Destroy(gameObject, 1);
        }
    }
}
