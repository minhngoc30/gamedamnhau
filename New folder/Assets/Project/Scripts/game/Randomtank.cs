using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Randomtank : MonoBehaviour
{
    public GameObject tanks, tankplane1, tankplane2, tankplane3, tankplane4, tankplane21, tankplane22, tankplane23, tankplane24;
    public float time=4;
    public float x, x1, y, y1;
    public Text text1;
    public Button continuee;

    void Update()
    {
        Vector3 random = new Vector3(Random.Range(x, x1), Random.Range(y, y1), 0f);
        if (time >0)
        {
            time -= 1;
            Instantiate(tanks, random, Quaternion.identity);
        }
        if (tank.tank1 >= 4)
        {
            text1.gameObject.SetActive(true);
            text1.text = "doi 1 win ";
            tankplane1.SetActive(true);
            tankplane2.SetActive(true);
            tankplane3.SetActive(true);
            tankplane4.SetActive(true);
            tankplane21.SetActive(false);
            tankplane22.SetActive(false);
            tankplane23.SetActive(false);
            tankplane24.SetActive(false);
            Time.timeScale = 0;
            continuee.gameObject.SetActive(true);
        }
        if (tank.tank2 >= 4)
        {
            text1.gameObject.SetActive(true);
            text1.text = "doi2 win ";
            tankplane1.SetActive(false);
            tankplane2.SetActive(false);
            tankplane3.SetActive(false);
            tankplane4.SetActive(false);
            tankplane21.SetActive(true);
            tankplane22.SetActive(true);
            tankplane23.SetActive(true);
            tankplane24.SetActive(true);
            Time.timeScale = 0;
            continuee.gameObject.SetActive(true);

        }
        if (tank.tank1 == 1) { tankplane1.SetActive(true); }
        if (tank.tank1 == 2) { tankplane2.SetActive(true); }
        if (tank.tank1 == 3) { tankplane3.SetActive(true); }
        if (tank.tank1 == 4) { tankplane4.SetActive(true); }
        if (tank.tank2 == 1) { tankplane21.SetActive(true); }
        if (tank.tank2 == 2) { tankplane22.SetActive(true); }
        if (tank.tank2 == 3) { tankplane23.SetActive(true); }
        if (tank.tank2 == 4) { tankplane24.SetActive(true); }
    }
    public void continu()
    {
        Time.timeScale = 1;
        if (tank.tank1==4)
            SceneManager.LoadScene("win1");
        if(tank.tank2==4)
            SceneManager.LoadScene("win2");
    }
}
