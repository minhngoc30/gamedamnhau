using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sound : MonoBehaviour
{
    public AudioClip axe, baseball, gunShort, gunLong, AK, run,item ,pldie;
    private AudioSource adisrc;
    // Start is called before the first frame update
    void Start()
    {
        axe = Resources.Load<AudioClip>("axe");
        baseball = Resources.Load<AudioClip>("baseball");
        gunShort = Resources.Load<AudioClip>("gunshort");
        gunLong = Resources.Load<AudioClip>("gunlong");
        AK = Resources.Load<AudioClip>("ak");
        run = Resources.Load<AudioClip>("run");
        item = Resources.Load<AudioClip>("item");
        pldie = Resources.Load<AudioClip>("pldie");
        adisrc = GetComponent<AudioSource>();
    }

    public void Playsound(string clip)
    {
        switch (clip)
        {
            case "axe":
                adisrc.clip = axe;
                adisrc.PlayOneShot(axe, 0.3f);
                break;
            case "baseball":
                adisrc.clip = baseball;
                adisrc.PlayOneShot(baseball, 0.3f);
                break;
            case "gunshort":
                adisrc.clip = gunShort;
                adisrc.PlayOneShot(gunShort, 0.3f);
                break;
            case "gunlong":
                adisrc.clip = gunLong;
                adisrc.PlayOneShot(gunLong, 0.3f);
                break;
            case "ak":
                adisrc.clip = AK;
                adisrc.PlayOneShot(AK, 0.3f);
                break;
            case "run":
                adisrc.clip = run;
                adisrc.PlayOneShot(run, 0.3f);
                break;
            case "item":
                adisrc.clip = item;
                adisrc.PlayOneShot(item, 0.3f);
                break; 
            case "pldie":
                adisrc.clip = pldie;
                adisrc.PlayOneShot(pldie, 0.3f);
                break;
           
        }
    }
}
