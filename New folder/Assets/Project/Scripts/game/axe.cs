using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class axe : MonoBehaviour
{
    private Animator anim;
    public Transform pl;
    private int noOfclick = 0;
    float lastCllickedTime = 0;
    private float maxComboDelay = 0.9f;
    public GameObject bullet1, bullet2;
    private Sound sound;

    // Start is called before the first frame update
    void Start()
    {
        sound = GameObject.FindGameObjectWithTag("sound").GetComponent<Sound>();

        anim = GetComponent<Animator>();
        if (MenuManage.player1 == 1 || MenuManage.player2 == 1 || MenuManage.player3 == 1 || MenuManage.player8 == 1)
            pl = GameObject.FindGameObjectWithTag("PlayerReal1").GetComponent<Transform>();
        if (MenuManage.player4 == 1 || MenuManage.player5 == 1 || MenuManage.player6 == 1 || MenuManage.player7 == 1)
            pl = GameObject.FindGameObjectWithTag("PlayerReal2").GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
       
        transform.position = pl.position;
        //attack
            if (Time.time - lastCllickedTime > maxComboDelay)
        {
            noOfclick = 0;
        }
        if (Input.GetMouseButtonDown(0))
        {
            lastCllickedTime = Time.time;
            noOfclick++;
            if (noOfclick == 1)
            {
                 anim.SetBool("Attack", true);
                if (MenuManage.gunindex0 == 1) sound.Playsound("axe");
                if (MenuManage.gunindex3 == 1) sound.Playsound("baseball");
            } 
            noOfclick = Mathf.Clamp(noOfclick, 0, 2);
            if (MenuManage.player1 == 1 || MenuManage.player2 == 1 || MenuManage.player3 == 1 || MenuManage.player8 == 1)
            {
                bullet1.SetActive(true); bullet2.SetActive(false);
            }
            if (MenuManage.player4 == 1 || MenuManage.player5 == 1 || MenuManage.player6 == 1 || MenuManage.player7 == 1)
            {
                bullet2.SetActive(true); bullet1.SetActive(false);
            }
        }
    }
    public void return1()
    {
        if (noOfclick == 2)
        {
            anim.SetBool("Attack2", true);
            if (MenuManage.gunindex0 == 1) sound.Playsound("axe");
            if (MenuManage.gunindex3 == 1) sound.Playsound("baseball");
        }

        else
        {
            anim.SetBool("Attack", false);
            noOfclick = 0;
        }
    }   
    public void return2()
    {
        anim.SetBool("Attack2", false);
        anim.SetBool("Attack", false);
        noOfclick = 0;
    }
   
}
