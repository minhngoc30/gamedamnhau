using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraShake : MonoBehaviour
{
    public static cameraShake instance;
    private float shakeTime, shakePower, shakeFadeTime, shakeRotation;

    public float rotationMultiplier;
    private void Start()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.K))
        {
            startShake(1f, 3f);
        }
    }
    private void LateUpdate()
    {
        if (shakeTime > 0)
        {
            shakeTime -= Time.deltaTime;
            float xAmout = Random.Range(-1f, 1f) * shakePower;
            float yAmout = Random.Range(-1f, -1f) * shakePower;
            transform.position += new Vector3(xAmout, yAmout, 0f);

            shakePower = Mathf.MoveTowards(shakePower, 0f, shakeFadeTime * Time.deltaTime);
            shakeRotation = Mathf.MoveTowards(shakeRotation, 0f, shakeFadeTime * rotationMultiplier * Time.deltaTime);
        }
        transform.rotation = Quaternion.Euler(0f,0f,shakeRotation*Random.Range(-3f,3f));
    }
    public void startShake(float length,float power)
    {
        shakeTime = length;
        shakePower = power;

        shakeFadeTime = power / length;

        shakeRotation = power * rotationMultiplier;
    }
   
}
