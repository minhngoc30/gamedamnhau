using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class destroyCoin : MonoBehaviour
{
  
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag=="PlayerReal1" || collision.gameObject.tag=="PlayerReal2")
        {
            Destroy(gameObject);
        }    
    }
}
