using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class limitTime : MonoBehaviour
{
    public   float startingTime = 300f , time=2;
    public GameObject panel;
    [SerializeField] Text limitText;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        startingTime -= 1 * Time.deltaTime;
        limitText.text = startingTime.ToString("0");
        if (startingTime <= 0)
        {
            startingTime = 0;
            panel.SetActive(true);
            time -= Time.deltaTime;
            if(time<0) SceneManager.LoadScene("loser");

        }
    }
}
