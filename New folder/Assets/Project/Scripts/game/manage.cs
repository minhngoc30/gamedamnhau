using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class manage : MonoBehaviour
{
    [SerializeField]
    private GameObject pausePanel;
    [SerializeField]
    private Button resumeGame;
    [SerializeField]
    private GameObject panel;
    public static float pl1 = 1, pl2 = 0, pl3 = 0;
    public Button buy;
    public GameObject Opentmap,minimap;
    public void Update()
    {
        if (coin.moneyAmount >= 5) buy.gameObject.SetActive(true); else buy.gameObject.SetActive(false);
    }
    public void PauseGame()
    {
        Time.timeScale = 0f;
        pausePanel.SetActive(true);
        resumeGame.onClick.RemoveAllListeners();
        resumeGame.onClick.AddListener(() => ResumeGame());
    }
    public void ResumeGame()
    {
        Time.timeScale = 1f;
        pausePanel.SetActive(false);
    }
    public void shop()
    {
        panel.SetActive(true);
        Time.timeScale = 0f;
    }
    public void closeShop()
    {
        panel.SetActive(false);
    }
    public void buyminimap()
    {
        coin.moneyAmount -= 5;
        Opentmap.gameObject.SetActive(true);
      //  buy.gameObject.SetActive(false);
    }
    public void opentMap()
    {
        minimap.gameObject.SetActive(true);
        Time.timeScale = 0f;
    }
    public void closeMap()
    {
        minimap.gameObject.SetActive(false);
        Time.timeScale = 1f;
    }
      public void selectPlayer()
    {
        SceneManager.LoadScene("MenuManage");
        Time.timeScale = 1f;
    }
    public void menu()
    {
        SceneManager.LoadScene("menu");
        Time.timeScale = 1f;
    }
}
