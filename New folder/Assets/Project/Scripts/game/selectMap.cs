using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class selectMap : MonoBehaviour
{
    public GameObject[] map;
    public int numberMap;
    // Start is called before the first frame update
    void Start()
    {
        numberMap = 0;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = map[numberMap].transform.position;
    }
    public void next()
    {
        numberMap += 1;
        if (numberMap == 4) numberMap = 0;
    }
    public void select()
    {
        if(numberMap==0) SceneManager.LoadScene("MenuManage");
        if(numberMap==1) SceneManager.LoadScene("MenuManage");
        if(numberMap==2) SceneManager.LoadScene("MenuManage");
        if(numberMap==3) SceneManager.LoadScene("MenuManage");
    }
}
