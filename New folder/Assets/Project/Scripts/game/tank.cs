using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tank : MonoBehaviour
{
    public float numbertank = 0, numnervitri;
    public float speed;
    public GameObject point1, point2;
    Vector2 vitri;
    public static int number, tank1=0, tank2=0;
    public static int DestroyPlane = 0;
    public static int DestroyPlane2 = 0;
    void Start()
    {
        point1 = GameObject.FindGameObjectWithTag("point1");
        point2 = GameObject.FindGameObjectWithTag("point2");
    }

    void Update()
    {
        FinePlayer();
        if (DestroyPlane == 0)
        {
            if (numnervitri == 1)
            {
                vitri = point1.transform.position;
                transform.position = Vector2.MoveTowards(transform.position, vitri, speed * Time.deltaTime);
                speed = 50;
            }
        }
        else if (DestroyPlane > 0) {
            vitri = point2.transform.position;
            transform.position = Vector2.MoveTowards(transform.position, vitri, speed * Time.deltaTime);
            speed = 50;
        }
        if (DestroyPlane2 == 0)
        {
            if (numnervitri == 2)
            {
                vitri = point2.transform.position;
                transform.position = Vector2.MoveTowards(transform.position, vitri, speed * Time.deltaTime);
                speed = 50;
            }
        }
        else if (DestroyPlane2 > 0)
        {
            vitri = point1.transform.position;
            transform.position = Vector2.MoveTowards(transform.position, vitri, speed * Time.deltaTime);
        }
       
    }
    void FinePlayer()
    {
        float distancePlayer = Mathf.Infinity;
        if (MenuManage.player1 == 1 || MenuManage.player2 == 1 || MenuManage.player3 == 1 || MenuManage.player8 == 1)
        {
            pl closestPlayer = null;
            pl[] allAnemy = GameObject.FindObjectsOfType<pl>();

            foreach (pl currentPlayer in allAnemy)
            {
                float distanceToPlayer = (currentPlayer.transform.position - this.transform.position).sqrMagnitude;
                if (distanceToPlayer < distancePlayer)
                {
                    distancePlayer = distanceToPlayer;
                    closestPlayer = currentPlayer;
                    vitri = closestPlayer.transform.position;
                }
            }
        }
        if (MenuManage.player4 == 1 || MenuManage.player5 == 1 || MenuManage.player6 == 1 || MenuManage.player7 == 1)
        {
            pl2 closestPl2 = null;
            pl2[] allpl2 = GameObject.FindObjectsOfType<pl2>();

            foreach (pl2 currentpl2 in allpl2)
            {
                float distanceToPlayer = (currentpl2.transform.position - this.transform.position).sqrMagnitude;
                if (distanceToPlayer < distancePlayer)
                {
                    distancePlayer = distanceToPlayer;
                    closestPl2 = currentpl2;
                    vitri = closestPl2.transform.position;
                }
            }
        }
        PlayerAI1 closestPlayer1 = null;
        PlayerAI1[] allPlayer1 = GameObject.FindObjectsOfType<PlayerAI1>();

        foreach (PlayerAI1 currentPlayer1 in allPlayer1)
        {
            float distanceToPlayer1 = (currentPlayer1.transform.position - this.transform.position).sqrMagnitude;
            if (distanceToPlayer1 < distancePlayer)
            {
                distancePlayer = distanceToPlayer1;
                closestPlayer1 = currentPlayer1;
                vitri = closestPlayer1.transform.position;
            }
        }     
        PlayerAI2 closestPlayer2 = null;
        PlayerAI2[] allPlayer2 = GameObject.FindObjectsOfType<PlayerAI2>();

        foreach (PlayerAI2 currentPlayer in allPlayer2)
        {
            float distanceToPlayer2 = (currentPlayer.transform.position - this.transform.position).sqrMagnitude;
            if (distanceToPlayer2 < distancePlayer)
            {
                distancePlayer = distanceToPlayer2;
                closestPlayer2 = currentPlayer;
                vitri = closestPlayer2.transform.position;
            }
        } 
        if (numbertank == 2 ||numbertank==1)
        {
            speed = 50;
            transform.position = Vector2.MoveTowards(transform.position, vitri, speed * Time.deltaTime);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player1" &&speed==0 &&numbertank==0||collision.gameObject.tag == "PlayerReal1" && speed==0 &&numbertank==0)
        {
            numbertank = 1;
        }
        if (collision.gameObject.tag == "PlayerReal2" && speed == 0&&numbertank==0 || collision.gameObject.tag == "Player2" && speed == 0&&numbertank==0)
        {
            numbertank = 2;
            Debug.Log("2");
        }
        if (collision.gameObject.tag == "point1" && numbertank == 1 )
        {
            numnervitri = 1;
            number += 1;
            tank1 += 1;
           
        }
        if (collision.gameObject.tag == "point2"  && numbertank == 2 )
        {
            numnervitri = 2;
            number += 1;
            tank2 += 1;
            Debug.Log("point");
         
        }
        if (DestroyPlane2 > 0)
        {
            if (collision.gameObject.tag == "point1" )
            {
                tank1 += 1;
              
            }
        }
        if (DestroyPlane >0)
        {
            if (collision.gameObject.tag == "point2" )
            {
                tank2 += 1;
                
            }
        }
        
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player1" && speed == 50 && numbertank == 1 || collision.gameObject.tag == "PlayerReal1" && speed == 50 && numbertank == 1)
        {
            numbertank = 0;
            speed = 0;
        }
        if (collision.gameObject.tag == "PlayerReal2" && speed == 50 && numbertank == 2 || collision.gameObject.tag == "Player2" && speed == 50 && numbertank == 2)
        {
            numbertank = 0;
            speed = 0;
        }
       
    }

}
