using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gamemanage : MonoBehaviour
{
    [SerializeField]
    private GameObject[] characterPrefabs;
    [SerializeField]
    private GameObject[] GunPrefabs;
    public GameObject IMplayer1, IMplayer2, IMplayer3, IMplayer4, IMplayer5, IMplayer6, IMplayer7, IMplayer8;
    // Start is called before the first frame update
    void Start()
    {
        LoadCharacter();
    }

    // Update is called once per frame
    void Update()
    {
        if (MenuManage.player1 == 1) IMplayer1.gameObject.SetActive(false);
        if (MenuManage.player2 == 1) IMplayer2.gameObject.SetActive(false);
        if (MenuManage.player3 == 1) IMplayer3.gameObject.SetActive(false);
        if (MenuManage.player4 == 1) IMplayer4.gameObject.SetActive(false);
        if (MenuManage.player5 == 1) IMplayer5.gameObject.SetActive(false);
        if (MenuManage.player6 == 1) IMplayer6.gameObject.SetActive(false);
        if (MenuManage.player7 == 1) IMplayer7.gameObject.SetActive(false);
        if (MenuManage.player8 == 1) IMplayer8.gameObject.SetActive(false);

    }
    private void LoadCharacter()
    {
        int characterIndex = PlayerPrefs.GetInt("CharacterIndex");
        Instantiate(characterPrefabs[characterIndex]);
        int GunIndex = PlayerPrefs.GetInt("GunIndex");
        Instantiate(GunPrefabs[GunIndex]);
    }
}
