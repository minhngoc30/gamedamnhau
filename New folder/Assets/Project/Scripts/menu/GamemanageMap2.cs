using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamemanageMap2 : MonoBehaviour
{
    [SerializeField]
    private GameObject[] characterPrefabs;
    [SerializeField]
    private GameObject[] GunPrefabs;
    // Start is called before the first frame update
    void Start()
    {
        LoadCharacter();
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void LoadCharacter()
    {
        int characterIndex = PlayerPrefs.GetInt("CharacterIndex");
        Instantiate(characterPrefabs[characterIndex]);
        int GunIndex = PlayerPrefs.GetInt("GunIndex");
        Instantiate(GunPrefabs[GunIndex]);
    }
}
