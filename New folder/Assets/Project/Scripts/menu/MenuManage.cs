using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManage : MonoBehaviour
{
    [SerializeField]
    private GameObject[] characters; 
    [SerializeField]
    private GameObject[] gun;
    private int characterIndex;
    private int GunIndex;
    public static int player1;
    public static int player2;
    public static int player3;
    public static int player4;
    public static int player5;
    public static int player6;
    public static int player7;
    public static int player8;
    public static int gunindex1;
    public static int gunindex2;
    public static int gunindex4;
    public static int gunindex0;
    public static int gunindex3;
    public Button pl1 ,pl2,pl3,pl4,pl5,pl6,pl7,pl8 , gun1,gun2,gun3,gun4,gun5;
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        if (characterIndex == 0) { player1 = 1; player2 = 0; player3 = 0; player4 = 0; player5 = 0; player6 = 0; player7 = 0; player8 = 0; }
        if (characterIndex == 1) { player1 = 0; player2 = 1; player3 = 0; player4 = 0; player5 = 0; player6 = 0; player7 = 0; player8 = 0; }
        if (characterIndex == 2) { player1 = 0; player2 = 0; player3 = 1; player4 = 0; player5 = 0; player6 = 0; player7 = 0; player8 = 0; }
        if (characterIndex == 3) { player1 = 0; player2 = 0; player3 = 0; player4 = 1; player5 = 0; player6 = 0; player7 = 0; player8 = 0; }
        if (characterIndex == 4) { player1 = 0; player2 = 0; player3 = 0; player4 = 0; player5 = 1; player6 = 0; player7 = 0; player8 = 0; }
        if (characterIndex == 5) { player1 = 0; player2 = 0; player3 = 0; player4 = 0; player5 = 0; player6 = 1; player7 = 0; player8 = 0; }
        if (characterIndex == 6) { player1 = 0; player2 = 0; player3 = 0; player4 = 0; player5 = 0; player6 = 0; player7 = 1; player8 = 0; }
        if (characterIndex == 7) { player1 = 0; player2 = 0; player3 = 0; player4 = 0; player5 = 0; player6 = 0; player7 = 0; player8 = 1; }
        if (GunIndex == 1) { gunindex1 = 1;gunindex2 = 0;gunindex4 = 0; }
        if (GunIndex == 2) { gunindex1 = 0; gunindex2 = 1; gunindex4 = 0; }
        if (GunIndex == 4) { gunindex1 = 0; gunindex2 = 0; gunindex4 = 1; }
        if (GunIndex == 0) { gunindex0 = 1; gunindex3 = 0; }
        if (GunIndex == 3) { gunindex0 = 0; gunindex3 = 1;}
    }
    public void ChangerCharater(int index)
    {
        for (int i = 0; i < characters.Length; i++)
        {
            characters[i].SetActive(false);
           
        }
        this.characterIndex = index;
        characters[index].SetActive(true);
    } 
    public void ChangerGun(int index)
    {
        for (int i = 0; i < gun.Length; i++)
        {
            gun[i].SetActive(false);
           
        }
        this.GunIndex = index;
        gun[index].SetActive(true);
    }
    public void StartGame()
    {
        SceneManager.LoadScene("PlayerTest");
        PlayerPrefs.SetInt("CharacterIndex", characterIndex);
        PlayerPrefs.SetInt("GunIndex", GunIndex);

    }
   public void StartGame2()
    {
        SceneManager.LoadScene("PlayerTest1");
        PlayerPrefs.SetInt("CharacterIndex", characterIndex);
        PlayerPrefs.SetInt("GunIndex", GunIndex);
    }
    public void Skip()
    {
        pl1.gameObject.SetActive(false);
        pl2.gameObject.SetActive(false);
        pl3.gameObject.SetActive(false);
        pl4.gameObject.SetActive(false);
        pl5.gameObject.SetActive(false);
        pl6.gameObject.SetActive(false);
        pl7.gameObject.SetActive(false);
        pl8.gameObject.SetActive(false);

        gun1.gameObject.SetActive(true);
        gun2.gameObject.SetActive(true);
        gun3.gameObject.SetActive(true);
        gun4.gameObject.SetActive(true);
        gun5.gameObject.SetActive(true);
      
    }
}
